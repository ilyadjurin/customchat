from django.db import models

from apps.users.models import Client


class ChatMessage(models.Model):
    """
    Chat message
    """
    user = models.ForeignKey(Client, on_delete=models.CASCADE)
    message = models.TextField(max_length=3000)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "chat message"
        verbose_name_plural = "chat messages"
        ordering = ['created']

    def __str__(self):
        return self.message

