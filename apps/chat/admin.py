from django.contrib import admin

from apps.chat.models import ChatMessage


@admin.register(ChatMessage)
class MessageAdmin(admin.ModelAdmin):
    model = ChatMessage
