from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.chat.api.serializers import MessageCreateSerializer
from apps.chat.models import ChatMessage


class MessageCreateView(ListCreateAPIView):
    """
    Create Message
    """
    permission_classes = (IsAuthenticated,)
    queryset = ChatMessage.objects.all()
    serializer_class = MessageCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        stack_mess = MessageCreateSerializer(self.get_queryset(), many=True)
        return Response(stack_mess.data, status=status.HTTP_201_CREATED, headers=headers)
