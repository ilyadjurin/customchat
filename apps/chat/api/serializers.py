from collections import OrderedDict

from django.db.models import QuerySet
from rest_framework import serializers
from rest_framework.fields import SkipField
from rest_framework.relations import PKOnlyObject

from apps.chat.models import ChatMessage


class MessageCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatMessage
        fields = ('user', 'message')

    def to_representation(self, instance):
        old = super(MessageCreateSerializer, self).to_representation(instance)
        if not isinstance(self.instance, QuerySet):
            return old
        else:
            ret = OrderedDict()
            fields = self._readable_fields

            for field in fields:
                try:
                    if isinstance(field.get_attribute(instance), PKOnlyObject):
                        attribute = instance.user.username
                    else:
                        attribute = field.get_attribute(instance)
                except SkipField:
                    continue
                check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
                if check_for_none is None:
                    ret[field.field_name] = None
                else:
                    ret[field.field_name] = attribute

            return ret
