import commands_factory from './commands_factory.js';

var app = angular.module('Chat', []);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

app.controller('BaseChatCtrl', ['$scope', '$rootScope', '$http', '$q', function ($scope, $rootScope, $http, $q) {

    $scope.message_list = [];
    $scope.SendMess = function () {
        if (typeof $scope.text_input != 'undefined') {

            $scope.message_list.push({'user': $rootScope.current_user.username, 'message': $scope.text_input});
            var data = {'user': $rootScope.current_user.id, 'message': $scope.text_input};
            commands_factory.chatFactory($http, $q).postMess(data).then((r) => {
                $scope.message_list = r;
            }, (e) => {
                $scope.text = error_mess;
                if (e.data.error) {
                    $scope.text = {'status': e.data.error};
                }
                console.error("ERROR", e.data);
            });
            $scope.text_input = '';
        }
    };

    $scope.GetUserAndMess = function () {
        $http({
            method: "GET",
            url: "/api/v1/users/user"
        }).then(function onFulfilledHandler(response) {
            $rootScope.current_user = response.data.data;
            return $rootScope.current_user
        });
        $http({
            method: "GET",
            url: "/api/v1/chat/create"
        }).then(function onFulfilledHandler(response) {
            $scope.message_list = response.data;
            return $scope.message_list
        });
    };

    $scope.GetUserAndMess();


}]);

