'use strict';


function chatFactory($http, $q) {
    function postMess(data) {
        let defer = $q.defer();
        $http.post('/api/v1/chat/create', data)
            .then((r) => {
                defer.resolve(r.data);
            }, (e) => {
                defer.reject(e);
            });
        return defer.promise;
    }

    return {
        postMess: postMess,
    }
}

export default {chatFactory}