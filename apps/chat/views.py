from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class ChatView(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    template_name = 'index.html'
