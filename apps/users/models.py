from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models


class Client(AbstractUser):
    name = models.CharField(max_length=100, blank=True, null=True)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+777777777777'."
                                         " Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, verbose_name="phone number", null=True)
