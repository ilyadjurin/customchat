from rest_framework import serializers

from apps.users.models import Client


class UsersBoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('phone_number', 'email')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"
