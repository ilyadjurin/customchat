from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import UserSerializer


class CurrentUserView(APIView):
    """
    Current User
    """
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        if not request.user.is_anonymous:
            serializer = UserSerializer(request.user)
            return Response({"data": serializer.data}, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_401_UNAUTHORIZED)
