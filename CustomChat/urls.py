from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

from apps.chat.views import ChatView

urlpatterns = [
    path('', ChatView.as_view()),
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('api/v1/users/', include('apps.users.api.urls')),
    path('api/v1/chat/', include('apps.chat.api.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
                   + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
