# CustomChat

Проект развернут на http://chat.autoton.biz


## Пользователи
Сейчас в системе (на сервере http://chat.autoton.biz) есть 2 пользователя.
- Админ(видит и может редактировать все) - admin
- Ответчик - john

Для всех пользователей установлен пароль: '1232testpass'

## Развертывание проекта
```
cp .env_default .env && nano .env
docker-compose build
docker-compose up
```

```